import org.apache.http.client.fluent.Request;
import org.json.JSONObject;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.CompletionStage;
import java.util.logging.Logger;

/**
 * Replace the comments marked as <code>{N}</code> placeholders with proper functions and your implementation.
 * <p>
 * <b>Note:</b><ul>
 * <li>no overhead, most of them expect just proper function name or could be implemented in 1-2 lines</li>
 * <li>try to follow functional style where it gives benefits (in your opinion)</li>
 * <li>no additional libraries/dependencies expected, only native JDK functions</li>
 * <li>(optionally) to test yourself you can build and run the project 
 * using gradle build config in the root of the repository</li>
 * </ul>
 */
public class Main {
    private final static Logger LOGGER = Logger.getLogger("Main");

    public static void main(final String[] args) {
        final CompletionStage</* {7} What type could be here?*/> twoJokesAsync = getTwoJokes()
                ./* {5} accept the result */ (twoJokes -> /* {6} print each joke using LOGGER*/);

        /* {8} block the application from exiting until the jokes are fetched and printed*/
    }

    private static CompletionStage<List<String>> getTwoJokes() {
        final CompletionStage<String> firstJokeAsync = /* {1} create an async non-blocking completion stage calling getChuckNorrisJoke)*/;
        final CompletionStage<String> secondJokeAsync = /* {2} create an async non-blocking completion stage calling getChuckNorrisJoke)*/;

        return /* {3} combine two stages into one*/.
        /* {4} aggregate two received string jokes to a list of jokes*/
    }

    /**
     * (<b>Slow</b>) thread blocking function to return Chuck Norris joke.
     * <p>
     * <b>DON'T CHANGE THIS METHOD</b>
     *
     * @return random Chuck Norris joke after (<b>slow</b>) URL request.
     */
    private static String getChuckNorrisJoke() {
        try {
            return new JSONObject(Request.Get("http://api.icndb.com/jokes/random")
                    .execute().returnContent().asString()).getJSONObject("value").getString("joke");
        } catch (IOException e) {
            throw new IllegalStateException("Smth is wrong: ", e);
        }
    }
}
